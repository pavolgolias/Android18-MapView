package sk.pgl.examples.android18_mapview;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

// Documentation descriptions:

// 1. https://developers.google.com/maps/documentation/android-api/current-places-tutorial - display info about current
//      and near places, custom marker, map state save, PROPER PERMISSION HANDLING, device location request

// 2. https://developers.google.com/maps/documentation/android-api/streetview - street view setup

// 3. custom styling of map - https://developers.google.com/maps/documentation/android-api/styling - base on various settings (e.g. in JSON) we can modify settings of map
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int MY_PERMISSION_REQUEST_LOCATION = 20;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        checkForPermissions();
    }

    private void checkForPermissions() {
        // it will be possible to show markers on the map, but it will be unable to get users position if we do not have ACCESS_FINE_LOCATION permission

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block this thread waiting for the user's
                // response! After the user sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
            }
        } else {
            initializeMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    initializeMap();
                } else {
                    // permission denied
                    Toast.makeText(getApplicationContext(), "You can not proceed without granted permission.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initializeMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        //addMarkersToMap();
        addHeatLayerToMap();
    }

    private void addHeatLayerToMap() {
        HeatmapTileProvider heatmapTileProvider = new HeatmapTileProvider.Builder()
                .data(DummyData.POSITIONS)
                .build();

        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
    }

    private void addMarkersToMap() {
        LatLngBounds.Builder boundBuilders = LatLngBounds.builder();
        for (LatLng latLng : DummyData.POSITIONS) {
            boundBuilders = boundBuilders.include(latLng);

            MarkerOptions options = new MarkerOptions().position(latLng).title("Position: " + latLng.toString());
            mMap.addMarker(options);
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DummyData.POSITIONS.get(0), 10f));
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(boundBuilders.build(), width, height, 20));
    }
}
